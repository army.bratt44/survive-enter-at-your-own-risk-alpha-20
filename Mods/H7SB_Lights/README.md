# How 7Days Should Be: Lights

**VERSION 2.0** - www.eihwaz.de - **CC BY-NC-ND-SA**

--- [**Video Preview**](https://www.youtube.com/watch?v=EmCS7Sb_LP8) (*click me*) ---

--- Credits: Doughs (Darkness) ---

--- --- --- --- --- --- --- --- ---

**Features**

H7SB Lights is a **server-side** mod that includes a reworked **light-system** and more lamps.

The mod has **additional modules** that can be optionally activated.

* **Rework: Light System**

	Light sources burn out after some time and can (usually) be reignited.
	
	To ignite a light source you need a flint/lighter and fuel.
	
	Torch		//	16 ticks	//	burns to ashes 	//	-
	
	Candle		//	32 ticks	//	re-ignitable	//	Animal fat
	
	Fire bowl	//	48 ticks	//	re-ignitable	//	Coal
	
	Lantern		//	64 ticks	//	re-ignitable	//	Gas
	
* **Rework: Lights**

	There are 20x 1-block lamps available via helper block.
	
	In addition, there are 5 large lamps and 2 furniture blocks.
	
	Can be crafted as a Rank 3 Engineer.

* **New: Fuse Box**

	Can be crafted as an engineer (3) and allow cables to be routed invisibly.
	
* **Rework: Flashlight**

	Flashlights require power, which they automatically draw from your equipment.
	
	The battery lasts for 5 minutes and automatically regenerates when switched off.
	
* **Rework: Helmet&Gun Flashlight**

	Mods and recipes only buyable at the trader.
	
* **Optional: Modules**
	
	-"xLights(Hardcore)" Removes overpowered head/gun flashlights and makes it darker.

--- --- --- --- --- --- --- --- ---

**Mod Installation**

1. Download and unpack the Zip data.

	Use "extract here" to prevent double packing.

2. Place it into your "Mods" folder.

	C:\Steam\steamapps\common\7 Days To Die

3. Have fun!

**Module Installation**

1. Go inside the h7sb mod folder.

	C:\Steam\steamapps\common\7 Days To Die\Mods\H7SB_Lights
	
2. Copy & paste (NOT drag & drop) the module you like into the main "Mods" folder.

	C:\Steam\steamapps\common\7 Days To Die\Mods

3. Have fun!

**Caution! There may be complications when integrating into existing saved games.**

--- --- --- --- --- --- --- --- ---

**Copyrights: Attribution-NonCommercial-NoDerivatives 4.0 International**

You are allowed to use this mod for your private use (singleplayer/multiplayer/server).

You are allowed to use this mod as part of your custom modpack (as separated mod).

Of cause your allow to take **inspiration** to improve your **existing** code!

Vehicle Name: Teleporting Pod

3D Model Artists Names: P3TroV

All Models in this modlet are subject to creative commons licence.
https://creativecommons.org/licenses/by/4.0/

No Models have been altered from the 'original creator' they are set up with an extra overlay texture and appropriate lighting shader in unity for 7 Days to Die game.

(ActiniumTiger)
Thank You To All Artists who post Free Assets for Modders to use in games.
Much appreciated!!!!
Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,japanese,german

stainlessSteelShapes:VariantHelper,blocks,Block,,,stainlessSteelShapes,,ステンレス形状,Formen aus rostfreiem Stahl
reinforceStainlessShapes:VariantHelper,blocks,Block,,,reinforceStainlessShapes,,強化ステンレス形状,Formen aus rostfreiem Stahl verstärken
completeStainlessShapes:VariantHelper,blocks,Block,,,completeStainlessShapes,,完全ステンレス形状,komplette Edelstahl-Formteile
orichalconShapes:VariantHelper,blocks,Block,,,orichalconShapes,,オリハルコン形状,Orichalcon-Formen

materialMstainlessSteel_shapes,,,,,stainlessSteelShapes,,ステンレス形状,Formen aus rostfreiem Stahl
materialMreinforceStainless_shapes,,,,,reinforceStainlessShapes,,強化ステンレス形状,Formen aus rostfreiem Stahl verstärken
materialMcompleteStainless_shapes,,,,,completeStainlessShapes,,完全ステンレス形状,komplette Edelstahl-Formteile
materialMorichalcon_shapes,,,,,orichalconShapes,,オリハルコン形状,Orichalcon-Formen
